<?php
require 'vendor/autoload.php';

$app = new \Slim\Slim(array(
    'templates.path' => 'vistas',
));

$app->container->singleton('log', function () {
    $log = new \Monolog\Logger('slim-skeleton');
    $log->pushHandler(new \Monolog\Handler\StreamHandler('logs/app.log', \Monolog\Logger::DEBUG));
    return $log;
});

$app->view(new \Slim\Views\Twig());
$app->view->parserOptions = array(
    'charset' => 'utf-8',
    'cache' => realpath('../templates/cache'),
    'auto_reload' => true,
    'strict_variables' => false,
    'autoescape' => true
);
$app->view->parserExtensions = array(new \Slim\Views\TwigExtension());

require "vendor/NotORM.php";

$BD_user="root";
$BD_pass='';
$pdo = new PDO('mysql:host=localhost;dbname=gestor;',$BD_user, $BD_pass);
$db = new NotORM($pdo);

$app->get('/', function () use ($app) {
	$app->redirect('index.php/tareas');    
});

$app->get('/tareas', function () use ($app, $db){
	$resultados=array();
	foreach ($db->tareas() as $resultado) {
        $resultados[]  = array(
            "id" => $resultado["id"],
            "descripcion" => $resultado["descripcion"],
            "estado" => $resultado["estado"]
            );	
	}

	for($i=0; $i<count($resultados); ++$i){
	if ($resultados[$i]['estado']=='Realizado')
			$resultados[$i]['class']='alert alert-success';
		elseif ($resultados[$i]['estado']=='Pendiente')
				$resultados[$i]['class']='alert alert-warning';
		elseif ($resultados[$i]['estado']=='En progreso')
				$resultados[$i]['class']='alert alert-info';
	}
    
	return $app->render('tareas.html', array('resultado' => $resultados));
		
})->name('tareas');

$app->delete('/tareas', function () use ($app, $db){
    $tareas = $app->request()->post();
	$tareas = $db->tareas()->where("id", $tareas['id']);
    if ($tareas->fetch()) {
        $result = $tareas->delete();
	}
	$app->redirect('tareas'); 
});

$app->get("/tareas/:id", function ($id) use ($app, $db) {
    
    $tareas = $db->tareas()->where("id", $id);
    if ($data = $tareas->fetch()) {
		$resultados[0]=array(
							"id" => $data["id"],
							"descripcion" => $data["descripcion"],
							"estado" => $data["estado"]
							);
	if ($resultados[0]['estado']=='Realizado')
			$resultados[0]['class']='alert alert-success';
		elseif ($resultados[0]['estado']=='Pendiente')
				$resultados[0]['class']='alert alert-warning';
		elseif ($resultados[0]['estado']=='En progreso')
				$resultados[0]['class']='alert alert-info';
			
    return $app->render('tareas.html', array('resultado' => $resultados,
											'url' => $resultados[0]['id'] ));
	}
   
});

$app->get('/insertar', function() use ($app) {
	
	return $app->render('insertar.html', array());
})->name('insertar');

$app->post('/insertar', function() use ($app, $db){
	$tareas = $app->request()->post();
	array_pop($tareas);	
    $result = $db->tareas->insert($tareas);
	$app->redirect('tareas'); 	
})->name('insertar');

$app->get('/editar/:id', function($id) use ($app, $db){
	$tareas = $db->tareas()->where("id", $id);
    if ($data = $tareas->fetch()) {
							$id = $data["id"];
							$descripcion = $data["descripcion"];
							$estado = $data["estado"];
	}							
			
    return $app->render('insertar.html', array('descripcion' => $descripcion,
											'url_b' => $id, 
											'editar' => true,
											'estado'=> $estado					
											));	
})->name('editar');

$app->put('/editar/:id', function($id) use ($app, $db){
	
	$tareas = $db->tareas()->where("id", $id);
    if ($tareas->fetch()) {
		$put= $app->request()->put();
		array_pop($put);
		array_shift($put);
		$result = $tareas->update($put);
		}
	$app->redirect('../tareas');
});
$app->run();
?>